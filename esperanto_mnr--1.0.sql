\echo Use "CREATE EXTENSION esperanto_mnr" to load this file. \quit

CREATE FUNCTION eomnr_lexize(internal, internal, internal, internal)
  RETURNS internal AS 'MODULE_PATHNAME' LANGUAGE C STRICT;

CREATE TEXT SEARCH TEMPLATE esperanto_mnr_template (
  LEXIZE = eomnr_lexize
);

COMMENT ON TEXT SEARCH TEMPLATE esperanto_mnr_template IS 'Esperanto MNR stemmer';

CREATE TEXT SEARCH DICTIONARY esperanto_mnr (
  TEMPLATE = esperanto_mnr_template
);

COMMENT ON TEXT SEARCH DICTIONARY esperanto_mnr IS 'Esperanto MNR stemmer';

CREATE TEXT SEARCH CONFIGURATION esperanto (
  COPY = simple
);

ALTER TEXT SEARCH CONFIGURATION esperanto
  ALTER MAPPING FOR asciiword WITH esperanto_mnr;

ALTER TEXT SEARCH CONFIGURATION esperanto
  ALTER MAPPING FOR word WITH esperanto_mnr;

ALTER TEXT SEARCH CONFIGURATION esperanto
  ALTER MAPPING FOR hword_part WITH esperanto_mnr;

ALTER TEXT SEARCH CONFIGURATION esperanto
  ALTER MAPPING FOR hword_asciipart WITH esperanto_mnr;

ALTER TEXT SEARCH CONFIGURATION esperanto
  ALTER MAPPING FOR asciihword WITH esperanto_mnr;

ALTER TEXT SEARCH CONFIGURATION esperanto
  ALTER MAPPING FOR hword WITH esperanto_mnr;

COMMENT ON TEXT SEARCH CONFIGURATION esperanto IS 'basic Esperanto configuration';
