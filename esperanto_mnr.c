#include "postgres.h"

#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "commands/defrem.h"
#include "tsearch/ts_locale.h"
#include "tsearch/ts_utils.h"

PG_MODULE_MAGIC;

void _PG_init(void);
void _PG_fini(void);
PG_FUNCTION_INFO_V1(eomnr_lexize);


struct trie;

struct trie_entry
{
  char c;
  struct trie *branch;
};

struct trie
{
  int num_entries;
  struct trie_entry entries[];
};


static struct trie *new_trie(void)
{
  struct trie *const ret = malloc(sizeof (struct trie));
  ret->num_entries = 0;
  return ret;
}

static void delete_trie(struct trie *const trie)
{
  int i;

  if (trie == NULL) return;

  for (i = 0; i < trie->num_entries; i++)
    delete_trie(trie->entries[i].branch);

  free(trie);
}

static void add_to_trie(struct trie **trie_p, const char *word)
{
  int i;

again:
  if (*trie_p == NULL) return;

  for (i = 0; i < (*trie_p)->num_entries; i++)
  {
    if ((*trie_p)->entries[i].c == *word)
    {
      trie_p = &(*trie_p)->entries[i].branch;
      word++;
      goto again;
    }
  }

  (*trie_p)->num_entries++;
  *trie_p = realloc(*trie_p, (*trie_p)->num_entries * sizeof (struct trie_entry));

  (*trie_p)->entries[(*trie_p)->num_entries - 1].c = *word;

  if (*word == '\0')
  {
    (*trie_p)->entries[(*trie_p)->num_entries - 1].branch = NULL;
    return;
  }

  (*trie_p)->entries[(*trie_p)->num_entries - 1].branch = new_trie();

  trie_p = &(*trie_p)->entries[(*trie_p)->num_entries - 1].branch;
  word++;

  goto again;
}

static bool search_trie(const struct trie *trie, const char *word)
{
  int i;

again:
  if (trie == NULL) return true;

  for (i = 0; i < trie->num_entries; i++)
  {
    if (trie->entries[i].c == *word)
    {
      trie = trie->entries[i].branch;
      word++;
      goto again;
    }
  }

  return false;
}


/* This list contains both valid words which, when stemmed, would become
   invalid, and invalid words which, when stemmed, would become valid. */
static const char *const special_words[] = {
  "amen",
  "disden",
  "ekden",
  "ilas",
  "ilis",
  "ilos",
  "ilu",
  "ilus",
  "l'",
  "la",
  "l’",
  "malplas",
  "malplis",
  "malplos",
  "malplu",
  "malplus",
  "maltr'",
  "maltr’",
  "maltroj",
  "maltron",
  "maltrojn",
  "minus",
  "onas",
  "onis",
  "onos",
  "onu",
  "onus",
  "tamen",
  "unu",
  NULL
};

struct trie *special_words_trie;


void
_PG_init(void)
{
  const char *const *special_word_p;

  special_words_trie = new_trie();

  for (special_word_p = &special_words[0]; *special_word_p != NULL; special_word_p++)
    add_to_trie(&special_words_trie, *special_word_p);
}

void
_PG_fini(void)
{
  delete_trie(special_words_trie);
}


static bool
is_stop_word(const char *const word)
{
  static const char *const stop_words[] = {
    "l'",
    "la",
    "l’",
    NULL
  };

  const char *const *stop_word_p;

  for (stop_word_p = &stop_words[0]; *stop_word_p != NULL; stop_word_p++)
    if (strcmp(word, *stop_word_p) == 0)
      return true;

  return false;
}

static bool
is_special_word(const char *const word)
{
  return search_trie(special_words_trie, word);
}

static bool
is_step2_root_word(const char *const word)
{
  /* TODO; there are 1100+ of these */
  return false;
}

static bool
is_correlative_root(const char *const word, const int len)
{
  if (len < 1 || word[len-1] != 'i')
    return false;

  switch (len-1)
  {
  case 0:  /* i- */
    return true;
  case 1:  /* ki-, ti- */
    return word[0] == 'k' || word[0] == 't';
  case 2:  /* ali-, ĉi- */
    return (word[0] == 'a' && word[1] == 'l') ||
      (word[0] == '\xC4' && word[1] == '\x89');
  case 3:  /* neni- */
    return word[0] == 'n' && word[1] == 'e' && word[2] == 'n';
  case 4:  /* kelki- */
    return word[0] == 'k' && word[1] == 'e' && word[2] == 'l' && word[3] == 'k';
  default:
    return false;
  }
}

static int
first_vowel(const char *const word)
{
  return strcspn(word, "aeiou");
}


Datum
eomnr_lexize(PG_FUNCTION_ARGS)
{
  const char *const in = (const char *) PG_GETARG_POINTER(1);
  const int32 length = PG_GETARG_INT32(2);
  char *word = lowerstr_with_len(in, length);
  TSLexeme *const res = palloc0(sizeof(TSLexeme) * 2);
  int len, fv;

  /* we want UTF-8 */
  {
    char *const recoded = pg_server_to_any(word, strlen(word), PG_UTF8);
    if (recoded != word)
    {
      pfree(word);
      word = recoded;
    }
  }

  len = strlen(word);
  fv = first_vowel(word);

  /* General rule: never make a valid word out of an invalid one.
     This permits use for e.g. spellchecking. */

  if (len == 0 || is_stop_word(word))
    pfree(word);
  else
  {
    if (len >= 2 && !is_special_word(word))
    {
      /* step 1: canonicalize endings */

      /* Invariant: len is never reduced to 0,
         and is never reduced below 2 until -n is removed. */

      switch (word[len-1])
      {
      case '\'':
        /* Replace -' with -o.
           Exclude -i(o) and roots with zero syllables. */
        if (fv < len-1 && !is_correlative_root(word, len-1))
          word[len-1] = 'o';
        break;

      case '\x99':
        /* Replace -’ with -o. 
           Exclude -i(o) and roots with zero syllables. */
        if (word[len-2] == '\x80' && fv < len-3 && word[len-3] == '\xE2' &&
            !is_correlative_root(word, len-3))
        {
          word[len-3] = 'o';
          len -= 2;
        }
        break;

      case 's':
        /* Replace -as, -is, -os, -us with -i.
           Exclude roots with zero syllables. */
        if (fv < len-2 &&
            (word[len-2] == 'a' || word[len-2] == 'i' || word[len-2] == 'o' || word[len-2] == 'u'))
        {
          word[len-2] = 'i';
          len--;
        }
        break;

      case 'u':
        /* Replace -u with -i.
           Exclude -iu and roots with zero syllables. */
        if (fv < len-1 && !is_correlative_root(word, len-1))
          word[len-1] = 'i';
        break;

      case 'n':
        /* Remove -jn from nouns, adjectives, and -iu.
           Exclude roots with zero syllables. */
        if (word[len-2] == 'j' && fv < len-3 &&
            (word[len-3] == 'o' || word[len-3] == 'a' ||
              (word[len-3] == 'u' && is_correlative_root(word, len-3))))
          len -= 2;

        /* Remove -n from nouns, adjectives, adverbs, and -iu.
           Exclude roots with zero syllables. */
        else if (fv < len-2 &&
            (word[len-2] == 'o' || word[len-2] == 'a' || word[len-2] == 'e' ||
              (word[len-2] == 'u' && is_correlative_root(word, len-2))))
          len--;

        break;

      case 'j':
        /* Remove -j from nouns and adjectives, and -iu.
           Exclude roots with zero syllables. */
        if (fv < len-2 &&
            (word[len-2] == 'o' || word[len-2] == 'a' ||
              (word[len-2] == 'u' && is_correlative_root(word, len-2))))
          len--;
        break;
      }

      word[len] = '\0';
    }

    if (len >= 5 && !is_step2_root_word(word))
    {
      /* step 2: canonicalize participles */

      int idx;

      /* Invariant: len is always >= 5. */

      if (word[len-2] == 't' &&
        (word[len-1] == 'a' || word[len-1] == 'e' || word[len-1] == 'i' || word[len-1] == 'o'))
      {
        if (word[len-3] == 'n')
          idx = len - 4;
        else
          idx = len - 3;

        /* Replace -int-, -ont-, -unt- with -ant-,
           and -it-, -ot-, -ut- with -at-.
           Exclude words with roots with zero syllables or with only one letter (a vowel). */
        if (idx >= 2 && (word[idx] == 'i' || word[idx] == 'o' || word[idx] == 'u') &&
          fv < idx)
          word[idx] = 'a';
      }
    }

    /* recode to server coding */
    {
      char *const recoded = pg_any_to_server(word, strlen(word), PG_UTF8);
      if (recoded != word)
      {
        pfree(word);
        word = recoded;
      }
    }

    res->lexeme = word;
  }

  PG_RETURN_POINTER(res);
}
