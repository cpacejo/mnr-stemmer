MODULES = esperanto_mnr
EXTENSION = esperanto_mnr
DATA = esperanto_mnr--1.0.sql

PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)
